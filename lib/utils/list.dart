import 'package:flutter/material.dart';

class KList {
  findWhere({
    @required List<dynamic> parent,
    @required String key,
    @required dynamic compare,
  }) {
    return parent.firstWhere((it) => it[key] == compare, orElse: () => null);
  }
}