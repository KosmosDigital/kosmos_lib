import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kosmos_lib/UI/style/container.dart';

class KInput extends StatefulWidget {
  ///
  /// Dimensions
  ///
  final double width;
  final double height;

  ///
  /// Style
  ///
  final Color backgroundColor;
  final double leftTopRadius;
  final double rightTopRadius;
  final double leftBottomRadius;
  final double rightBottomRadius;
  final double radius;
  final double borderWidth;
  final Color borderColor;
  final double borderWidthTop;
  final double borderWidthBottom;
  final double borderWidthLeft;
  final double borderWidthRight;
  final double padding;
  final double leftPadding;
  final double rightPadding;
  final double topPadding;
  final double bottomPadding;
  final double margin;
  final double leftMargin;
  final double rightMargin;
  final double topMargin;
  final double bottomMargin;
  final List<BoxShadow> boxShadow;
  final Color borderColorTop;
  final Color borderColorBottom;
  final Color borderColorLeft;
  final Color borderColorRight;

  ///
  /// TextField
  ///
  final TextEditingController controller;
  final bool obscureText;
  final Color writeColor;
  final double writeSize;
  final String placeholder;
  final Color placeholderColor;
  final TextInputType keyboardType;
  final List<TextInputFormatter> inputFormatters;
  final int maxLines;
  final int minLines;
  final void Function(String) onChanged;
  final void Function() onEditingComplete;
  final void Function(String) onSubmitted;
  final Widget leftIcon;
  final double leftIconScale;
  final Widget rightIcon;
  final double rightIconScale;
  final void Function() leftIconOnPressed;
  final void Function() rightIconOnPressed;

  KInput({
    Key key,
    this.width = 200,
    this.height = 50,
    this.backgroundColor = Colors.white,
    this.leftTopRadius = 0,
    this.rightTopRadius = 0,
    this.leftBottomRadius = 0,
    this.rightBottomRadius = 0,
    this.radius = 0,
    this.borderWidth = 0,
    this.borderColor = Colors.transparent,
    this.borderWidthTop = 0,
    this.borderWidthBottom = 0,
    this.borderWidthLeft = 0,
    this.borderWidthRight = 0,
    this.padding = 0,
    this.leftPadding = 0,
    this.rightPadding = 0,
    this.topPadding = 0,
    this.bottomPadding = 0,
    this.margin = 0,
    this.leftMargin = 0,
    this.rightMargin = 0,
    this.topMargin = 0,
    this.bottomMargin = 0,
    @required this.controller,
    this.obscureText = false,
    this.writeColor = Colors.black87,
    this.writeSize = 18,
    this.placeholder,
    this.placeholderColor = Colors.grey,
    this.keyboardType = TextInputType.text,
    this.inputFormatters,
    this.maxLines = 1,
    this.minLines,
    this.onChanged,
    this.onEditingComplete,
    this.onSubmitted,
    this.boxShadow,
    this.borderColorTop = Colors.transparent,
    this.borderColorBottom = Colors.transparent,
    this.borderColorLeft = Colors.transparent,
    this.borderColorRight = Colors.transparent,
    this.leftIcon,
    this.leftIconScale = 1,
    this.rightIcon,
    this.rightIconScale = 1,
    this.leftIconOnPressed,
    this.rightIconOnPressed,
  }) : assert(controller != null),
  super(key: key);

  @override
  _KInputState createState() => _KInputState();
}

class _KInputState extends State<KInput> {

  @override
  Widget build(BuildContext context) {
    return new KContainer(
      width: widget.width,
      height: widget.height,
      backgroundColor: widget.backgroundColor,
      leftTopRadius: widget.leftTopRadius,
      rightTopRadius: widget.rightTopRadius,
      leftBottomRadius: widget.leftBottomRadius,
      rightBottomRadius: widget.rightBottomRadius,
      radius: widget.radius,
      borderWidth: widget.borderWidth,
      borderColor: widget.borderColor,
      borderWidthTop: widget.borderWidthTop,
      borderWidthBottom: widget.borderWidthBottom,
      borderWidthLeft: widget.borderWidthLeft,
      borderWidthRight: widget.borderWidthRight,
      padding: widget.padding,
      leftPadding: widget.leftPadding,
      rightPadding: widget.rightPadding,
      topPadding: widget.topPadding,
      bottomPadding: widget.bottomPadding,
      margin: widget.margin,
      leftMargin: widget.leftMargin,
      rightMargin: widget.rightMargin,
      topMargin: widget.topMargin,
      bottomMargin: widget.bottomMargin,
      boxShadow: widget.boxShadow,
      borderColorTop: widget.borderColorTop,
      borderColorBottom: widget.borderColorBottom,
      borderColorLeft: widget.borderColorLeft,
      borderColorRight: widget.borderColorRight,
      child: new Center(
        child: new TextField(
          controller: widget.controller,
          onChanged: widget.onChanged,
          onEditingComplete: widget.onEditingComplete,
          onSubmitted: widget.onSubmitted,
          style: TextStyle(
            color: widget.writeColor,
            fontSize: widget.writeSize,
          ),
          decoration: InputDecoration(
            icon: new GestureDetector(
              onTap: widget.leftIconOnPressed,
              child: new Container(
                width: (widget.height / widget.leftIconScale),
                height: (widget.height / widget.leftIconScale),
                child: widget.leftIcon,
              ),
            ),
            suffixIcon: new GestureDetector(
              onTap: widget.rightIconOnPressed,
              child: new Container(
                width: (widget.height / widget.rightIconScale),
                height: (widget.height / widget.rightIconScale),
                child: widget.rightIcon,
              ),
            ),
            border: InputBorder.none,
            hintText: widget.placeholder,
            hintStyle: TextStyle(
              fontSize: widget.writeSize,
              color: widget.placeholderColor,
            ),
          ),
          keyboardType: widget.keyboardType,
          obscureText: widget.obscureText,
          inputFormatters: widget.inputFormatters,
          maxLines: widget.maxLines,
          minLines: widget.minLines,
        ),
      ),
    );
  }
}