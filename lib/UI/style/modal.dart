import 'package:flutter/material.dart';

Future<T> kmodal<T>({
  @required BuildContext context,
  double radius = 10,
  double padding = 15,
  double width = 200,
  @required List<Widget> children,
  bool barrierDismissible = true,
}) {
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(radius))),
        contentPadding: EdgeInsets.all(padding),
        content: new Container(
          width: width,
          child: new Column(
            children: children,
          ),
        ),
      );
    },
    barrierDismissible: barrierDismissible,
  );
}