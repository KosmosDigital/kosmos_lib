import 'package:flutter/material.dart';

class KContainer extends StatefulWidget {
  ///
  /// Dimensions
  ///
  final double width;
  final double height;

  ///
  /// Style
  ///
  final Color backgroundColor;
  final double leftTopRadius;
  final double rightTopRadius;
  final double leftBottomRadius;
  final double rightBottomRadius;
  final double radius;
  final double borderWidth;
  final Color borderColor;
  final double borderWidthTop;
  final double borderWidthBottom;
  final double borderWidthLeft;
  final double borderWidthRight;
  final Color borderColorTop;
  final Color borderColorBottom;
  final Color borderColorLeft;
  final Color borderColorRight;
  final double padding;
  final double leftPadding;
  final double rightPadding;
  final double topPadding;
  final double bottomPadding;
  final double margin;
  final double leftMargin;
  final double rightMargin;
  final double topMargin;
  final double bottomMargin;
  final List<BoxShadow> boxShadow;

  ///
  /// Child
  ///
  final Widget child;

  KContainer({
    this.width = 200,
    this.height = 50,
    this.backgroundColor = Colors.white,
    this.leftTopRadius = 0,
    this.rightTopRadius = 0,
    this.leftBottomRadius = 0,
    this.rightBottomRadius = 0,
    this.radius = 0,
    this.borderWidth = 0,
    this.borderColor = Colors.transparent,
    this.borderWidthTop = 0,
    this.borderWidthBottom = 0,
    this.borderWidthLeft = 0,
    this.borderWidthRight = 0,
    this.padding = 0,
    this.leftPadding = 0,
    this.rightPadding = 0,
    this.topPadding = 0,
    this.bottomPadding = 0,
    this.margin = 0,
    this.leftMargin = 0,
    this.rightMargin = 0,
    this.topMargin = 0,
    this.bottomMargin = 0,
    this.boxShadow,
    this.borderColorTop = Colors.transparent,
    this.borderColorBottom = Colors.transparent,
    this.borderColorLeft = Colors.transparent,
    this.borderColorRight = Colors.transparent,
    @required this.child,
  });

  @override
  _KContainerState createState() => _KContainerState();
}

class _KContainerState extends State<KContainer> {

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: widget.width,
      height: widget.height,
      padding: widget.padding != 0 ? new EdgeInsets.all(widget.padding) : new EdgeInsets.only(
        top: widget.topPadding,
        left: widget.leftPadding,
        right: widget.rightPadding,
        bottom: widget.bottomPadding,
      ),
      margin: widget.padding != 0 ? new EdgeInsets.all(widget.margin) : new EdgeInsets.only(
        top: widget.topMargin,
        left: widget.leftMargin,
        right: widget.rightMargin,
        bottom: widget.bottomMargin,
      ),
      decoration: new BoxDecoration(
        color: widget.backgroundColor,
        borderRadius: widget.radius != 0 ? new BorderRadius.all(new Radius.circular(widget.radius)) : widget.leftTopRadius > 0 ||
        widget.rightTopRadius > 0 || widget.leftBottomRadius > 0 || widget.rightBottomRadius > 0 ? new BorderRadius.only(
          topLeft: new Radius.circular(widget.leftTopRadius),
          topRight: new Radius.circular(widget.rightTopRadius),
          bottomLeft: new Radius.circular(widget.leftBottomRadius),
          bottomRight: new Radius.circular(widget.rightBottomRadius),
        ) : null,
        border: widget.borderWidth != 0 ? new Border.all(width: widget.borderWidth, color: widget.borderColor) : widget.borderWidthTop > 0 ||
        widget.borderWidthBottom > 0 || widget.borderWidthLeft > 0 || widget.borderWidthRight > 0 ? new Border(
          top: new BorderSide(width: widget.borderWidthTop, color: widget.borderColorTop),
          bottom: new BorderSide(width: widget.borderWidthBottom, color: widget.borderColorBottom),
          left: new BorderSide(width: widget.borderWidthLeft, color: widget.borderColorLeft),
          right: new BorderSide(width: widget.borderWidthRight, color: widget.borderColorRight),
        ) : null,
        boxShadow: widget.boxShadow,
      ),
      child: widget.child,
    );
  }
}