import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class KLoader extends StatefulWidget {
  ///
  /// Style
  ///
  final Color color;
  final double size;
  final double width;
  final double height;

  KLoader({
    this.width = 50,
    this.height = 50,
    this.color = Colors.white,
    this.size = 20,
  });

  @override
  _KLoaderState createState() => _KLoaderState();
}

class _KLoaderState extends State<KLoader> with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return new SizedBox(
      width: widget.width,
      height: widget.height,
      child: SpinKitSquareCircle(
        color: widget.color,
        size: widget.size,
        controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1200)),
      ),
    );
  }
}