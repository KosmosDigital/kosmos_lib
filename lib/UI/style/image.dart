import 'package:flutter/material.dart';

class KImage extends StatefulWidget {
  final double width;
  final double height;
  final String image;
  final BoxFit fit;
  final double scale;

  KImage({
    Key key,
    this.width = 100,
    this.height = 100,
    @required this.image,
    this.fit = BoxFit.cover,
    this.scale = 1,
  }) : assert(image != null || image.isNotEmpty),
  super(key: key);

  @override
  _KImageState createState() => _KImageState();
}

class _KImageState extends State<KImage> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      child: RegExp(r'(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+').hasMatch(widget.image) ? Image.network(widget.image, fit: widget.fit, scale: widget.scale) : Image.asset(widget.image, fit: widget.fit, scale: widget.scale),
    );
  }
}