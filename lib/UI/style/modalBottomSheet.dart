import 'package:flutter/material.dart';

Future<T> kbottomModal<T>({
  @required BuildContext context,
  double height = 350,
  Color backgroundColor = Colors.white,
  double radius = 10,
  @required List<Widget> children,
}) {
  return showModalBottomSheet(
    context: context,
    builder: (BuildContext context) {
      return new Container(
        height: height,
        decoration: BoxDecoration(
          color: Colors.transparent,
        ),
        child: new Container(
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(radius),
              topRight: Radius.circular(radius),
            ),
          ),
          child: new SingleChildScrollView(
            child: new Column(
              children: children,
            ),
          ),
        ),
      );
    }
  );
}