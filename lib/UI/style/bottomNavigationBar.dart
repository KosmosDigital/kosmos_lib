import 'package:flutter/material.dart';
import 'package:kosmos_lib/UI/style/container.dart';

class KBottomNavigationBar extends StatefulWidget {
  ///
  /// Dimensions
  ///
  final double width;
  final double height;

  ///
  /// Style
  ///
  final Color backgroundColor;
  final double leftTopRadius;
  final double rightTopRadius;
  final double leftBottomRadius;
  final double rightBottomRadius;
  final double radius;
  final double borderWidth;
  final Color borderColor;
  final double borderWidthTop;
  final double borderWidthBottom;
  final double borderWidthLeft;
  final double borderWidthRight;
  final Color borderColorTop;
  final Color borderColorBottom;
  final Color borderColorLeft;
  final Color borderColorRight;
  final double padding;
  final double leftPadding;
  final double rightPadding;
  final double topPadding;
  final double bottomPadding;
  final double margin;
  final double leftMargin;
  final double rightMargin;
  final double topMargin;
  final double bottomMargin;
  final List<BoxShadow> boxShadow;

  ///
  /// Content
  ///
  final List<Widget> children;

  KBottomNavigationBar({
    Key key,
    this.width = 200,
    this.height = 50,
    this.backgroundColor = Colors.white,
    this.leftTopRadius = 0,
    this.rightTopRadius = 0,
    this.leftBottomRadius = 0,
    this.rightBottomRadius = 0,
    this.radius = 0,
    this.borderWidth = 0,
    this.borderColor = Colors.transparent,
    this.borderWidthTop = 0,
    this.borderWidthBottom = 0,
    this.borderWidthLeft = 0,
    this.borderWidthRight = 0,
    this.padding = 0,
    this.leftPadding = 0,
    this.rightPadding = 0,
    this.topPadding = 0,
    this.bottomPadding = 0,
    this.margin = 0,
    this.leftMargin = 0,
    this.rightMargin = 0,
    this.topMargin = 0,
    this.bottomMargin = 0,
    this.boxShadow,
    this.borderColorTop = Colors.transparent,
    this.borderColorBottom = Colors.transparent,
    this.borderColorLeft = Colors.transparent,
    this.borderColorRight = Colors.transparent,
    @required this.children,
  }) : assert(children != null),
  super(key: key);

  @override
  _KBottomNavigationBarState createState() => _KBottomNavigationBarState();
}

class _KBottomNavigationBarState extends State<KBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return KContainer(
      width: widget.width,
      height: widget.height,
      backgroundColor: widget.backgroundColor,
      leftTopRadius: widget.leftTopRadius,
      rightTopRadius: widget.rightTopRadius,
      leftBottomRadius: widget.leftBottomRadius,
      rightBottomRadius: widget.rightBottomRadius,
      radius: widget.radius,
      borderWidth: widget.borderWidth,
      borderColor: widget.borderColor,
      borderWidthTop: widget.borderWidthTop,
      borderWidthBottom: widget.borderWidthBottom,
      borderWidthLeft: widget.borderWidthLeft,
      borderWidthRight: widget.borderWidthRight,
      padding: widget.padding,
      leftPadding: widget.leftPadding,
      rightPadding: widget.rightPadding,
      topPadding: widget.topPadding,
      bottomPadding: widget.bottomPadding,
      margin: widget.margin,
      leftMargin: widget.leftMargin,
      rightMargin: widget.rightMargin,
      topMargin: widget.topMargin,
      bottomMargin: widget.bottomMargin,
      boxShadow: widget.boxShadow,
      borderColorTop: widget.borderColorTop,
      borderColorBottom: widget.borderColorBottom,
      borderColorLeft: widget.borderColorLeft,
      borderColorRight: widget.borderColorRight,
      child: new Row(
        children: widget.children,
      ),
    );
  }
}