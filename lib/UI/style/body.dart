import 'package:flutter/material.dart';

class Body extends StatefulWidget {
  ///
  /// Child
  ///
  final Widget child;

  Body({
    this.child,
  });

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {

  /* Dimensions */
  double width;
  double height;

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: new Container(
        width: width,
        height: height,
        child: widget.child,
      ),
    );
  }
}